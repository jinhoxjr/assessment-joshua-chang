import { Injectable } from "@angular/core";
import {HttpClient, HttpHeaders} from '@angular/common/http';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
};

@Injectable()
export class ServiceAPI {

    private backendURL = "mockurl";

    constructor(private http: HttpClient){}
    
    //mock
    postIdStatus(id: string, status: string){
        return this.http.post(this.backendURL, {id, status}, httpOptions)
        .pipe();
    }
}