import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {ServiceAPI} from '../service.server';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})
export class TableComponent implements OnInit {

  jsonData: any;
  private url = "http://localhost:4200/assets/"

  totalData: number;  //total Data
  currentPage = 0;
  rowLimit = 20; //max item per page
  pageSize: any; //list of pages

  displayData: any; //json to display

  constructor(public http:HttpClient, private serviceAPI: ServiceAPI ) {
    this.getJsonData();
  }

  getJsonData(){
    this.displayData = [];
    this.http.get(this.url + "sample_data.json")
        .subscribe(data => {
            this.jsonData = data;
            this.totalData = Object.keys(data).length;
            this.pageSize = Array.from(Array(Math.ceil(this.totalData/this.rowLimit)).keys());
            for(let i=(this.currentPage*this.rowLimit); i<((this.currentPage+1)*this.rowLimit); i++){
              if(data[i])
              this.displayData.push(data[i]);
            }
            console.log(data);
            console.log(this.pageSize);
            console.log("getPage(): ", this.displayData)
        }, error => console.error(error));
  }

  ngOnInit() {
  }

  gotoPage(p: number){
    this.currentPage = p;
    this.getJsonData();
  }

  nextPage(){
    this.currentPage++;
    this.getJsonData();
  }

  previousPage(){
    this.currentPage--;
    this.getJsonData();
  }
  
  setRowLimit(r: string){
    this.rowLimit = parseInt(r);
    this.currentPage = 0;
    this.getJsonData();
  }

  buttonPost(id: string, status: string){
    this.serviceAPI.postIdStatus(id, status)
    .subscribe((response) => {
      console.log("buttonPost() ", response)
    },
    (error) => {
      console.log("PostIdStatus() ", error)
    });
    alert("Mock Post complete...");
  }


}
